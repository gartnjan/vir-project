
import cv2

from tf_pose import common
import os

from tf_pose.estimator import TfPoseEstimator
from tf_pose.networks import get_graph_path, model_wh

def Get_number_of_dataset():
    '''
    Get number of images in ./dataset/
    :return: number of images
    '''

    dir = './data/im/'
    onlyfiles = next(os.walk(dir))[2]
    return len(onlyfiles)


if __name__ == '__main__':

    resize = '432x368'
    model = 'mobilenet_thin'

    w, h = model_wh(resize)
    e = TfPoseEstimator(get_graph_path(model), target_size=(w, h))

    f = open('data/body_parts.txt', "w+")

    with open('data/angles.txt', "r") as f2:
        lines = f2.read().splitlines()

    f3 = open('data/new_angles.txt', "w+")

    Img_count = Get_number_of_dataset()
    print('Pocet obrazku '+str(Img_count)+'\n\r')

    for img_idx in range (0, Img_count):
        picname = './data/im/' + str(img_idx) + '.jpg'
        image = common.read_imgfile(picname, None, None)



        humans = e.inference(image, resize_to_default=(w > 0 and h > 0), upsample_size=4.0)
        print('obrazek', img_idx)
        kontrola = 0
        for part in humans[0].body_parts:
            if part < 5 and part > 0:
                kontrola += 1
        if kontrola == 4:
            pixels = cv2.imread(picname)
            for part in humans[0].body_parts:
                if part < 5 and part > 0:
                    kontrola += 1
                    x = humans[0].body_parts[part][0]
                    y = humans[0].body_parts[part][1]
                    f.write(str(x) + ' ')
                    f.write(str(y) + ' ')
                    height, width, channels = pixels.shape
                    cv2.circle(pixels, (int(width * x), int(height * y)), 4, (0, 255, 0), -1)
            print('zarazen')
            cv2.imwrite(picname, pixels)
            f3.write(lines[img_idx-1]+'\r\n')
            f.write('\r\n')

        cv2.destroyAllWindows()
    f3.close()
    f.close()
    f2.close()
    cv2.destroyAllWindows()
