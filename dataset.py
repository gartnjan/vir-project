
import time
from utils import countdown
import pickle
import os.path
import numpy as np
import cv2
import random
from kukamanip import Kuka
import pybullet as p
import pybullet_data

class Dataset:
    def __init__(self):
        print("Checking for existing dataset...")
        # coordinates in picture
        filename = "data/body_parts.txt"
        if os.path.exists(filename):
            self.COORDINATES_F = open(filename)
            self.coordinates = []
            temp = self.COORDINATES_F.read().splitlines()
            for line in temp:
                self.coordinates.append(list(map(float, line.split())))
        else:
            print("No dataset found")
            self.coordinates = {}
            self.M = 0

        # robot angles
        filename = "data/new_angles.txt"
        if os.path.exists(filename):
            self.ANGLES_F = open(filename)
            self.angles = []
            temp = self.ANGLES_F.read().splitlines()
            for line in temp:
                self.angles.append(list(map(float, line.split())))


            self.M = len(self.coordinates)
        else:
            print("No dataset found")
            self.angles = {}
            self.M = 0

    def gather(self, N, ROB):
        print("Gathering new dataset")

        cam = cv2.VideoCapture(0)

        start_pos = [np.pi/4, np.pi/2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        fd = open("data/angles.txt", 'w')



        countdown(3)
        end_pos = [0,0,0,0,0,0,0,0,0,0,0,0]
        for i in range(N):
            print("Robot motion will now be played 3 times. ")
            end_pos[0] = start_pos[0]
            end_pos[1] = start_pos[1] + random.uniform(-np.pi/2,np.pi/2);
            end_pos[2] = start_pos[2]
            end_pos[3] = start_pos[3] + random.uniform(0, np.pi / 2);

            ROB.set_state(end_pos);
            for j in range(100):
                ret_val, image = cam.read()
                image = cv2.resize(image,(432,368))
                image_flip = cv2.flip(image, 1)
                cv2.imshow('image', image_flip)
                cv2.waitKey(1)

            print('image captured')
            image = cv2.resize(image, (432, 368))
            image_flip = cv2.flip(image, 1)
            cv2.imshow('image', image_flip)
            cv2.waitKey(1)
            fname = 'data/im/' + str(i) + '.jpg';
            print(fname)
            cv2.imwrite(fname, image_flip)
            for j in range(4):
                fd.write(str(end_pos[j]) + ' ')
            fd.write('\n')

        fd.close()
        cam.release()
        cv2.destroyAllWindows()



        #     print("Human motion will start recording. ")
        #
        #     countdown(3)
        #
        #     traj = []
        #     for k in range(len):
        #         ROB.set_state(mot[k])
        #         ROB.render()
        #         img = CAM.render(self.res)
        #         traj.append((mot[k], img))
        #         time.sleep(motionSleep)
        #
        #     self.dataset[i + self.M] = traj
        #
        # pickle.dump(self.dataset, open("data/trainingset", "wb"))
        # self.M += N

    def getBatch(self, N):
        # assert len(self.coordinates) > N, print("Dataset inadequate or empty")
        assert len(self.coordinates) == len(self.angles), print("Difference in number of lines")
        coords = []; angles = []

        for i in range(N):
            random_index = random.randrange(len(self.coordinates))
            coords.append(self.coordinates[random_index])
            angles.append(self.angles[random_index])

        coords = np.array(coords, dtype=np.float32)
        angles = np.array(angles, dtype=np.float32)

        print(coords)

        return coords, angles

    def getRndTraj(self):
        raise NotImplementedError

    def getTrajBatch(self):
        raise NotImplementedError









'''

class GRDataset:
    def __init__(self, name, gestures, res=(128,128), gs=True):
        self.name = name
        self.gestures = gestures
        self.res = res
        self.gs = gs

        print("Checking for existing dataset...")
        filename = "data/{}".format(self.name)
        if os.path.exists(filename):
            self.dataset = pickle.load(open(filename, "rb"))
            total = 0
            for v in self.dataset.values():
                total += len(v)
            print("Found existing dict with {} examples.".
                  format(total))
        else:
            print("No dataset found")
            self.dataset = {}


    def gather(self, N, CAM, gestureID):
        assert N > 0, print("You specified zero examples to be gathered.")

        print("Gathering gesture...")

        examples = []
        for k in range(N):
            img = CAM.render(self.res)
            examples.append(img)

        if gestureID in self.dataset:
            self.dataset[gestureID].extend(examples)
        else:
            self.dataset[gestureID] = examples

        print("Saving dataset")
        pickle.dump(self.dataset, open("data/{}".format(self.name), "wb"))


    def getBatch(self, N):
        """

        :param N: int, batchsize
        :return: [ndarray, ndarray], Returns list of N normalized float images with shape Nxhxw and cats with shape Nx7
        """
        ims = []
        cats = []

        n_categories = len(self.dataset)
        for i in range(N):
            cat = np.random.randint(0, n_categories)
            cat_ims = self.dataset[cat]
            img = cat_ims[np.random.randint(len(cat_ims))]
            img = cv2.normalize(img.astype('float32'), None, 0.0, 1.0, cv2.NORM_MINMAX)
            ims.append(img)
            cats.append(cat)

        ims = np.array(ims)
        cats = np.array(cats)

        return ims, cats


if __name__ == "__main__":
    jaco = Dataset()
    print(jaco.coordinates)
    print(jaco.angles)


'''