import numpy as np
import matplotlib.pyplot as plt
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF
import time

import matplotlib as mpl
mpl.rcParams['text.usetex'] = True
mpl.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']

class GProc:
    def __init__(self, dim, len, ls=2.0, div=3.0):
        self.dim = dim
        self.len = len
        self.ls = ls
        self.div = div

        self.kernel = 1.0 * RBF(length_scale=ls,
                                length_scale_bounds=(1e-1, 10.0))
        self.gp = GaussianProcessRegressor(kernel=self.kernel)
        self.sample = self.sample_gp(len)
        self.step = 0


    def sample_gp(self, length):
        timeline = np.arange(0, length, 1)
        sample = self.gp.sample_y(timeline[:, np.newaxis],
                                  n_samples=self.dim,
                                  random_state=None)

        sample /= self.div
        return sample.transpose()


    def __call__(self):
        if self.step >= self.len:
            self.reset()
        sample = self.sample[:, self.step]
        self.step += 1
        return sample


    def reset(self):
        self.step = 0
        self.sample = self.sample_gp(self.len)


    def plotsample(self, sample):
        fig = plt.figure()

        for i in range(self.dim):
            plt.plot(np.arange(sample.shape[1]), sample[i, :])
        plt.show()


def countdown(s):
    print("Counting...")
    for i in range(s):
        print("{}".format(s-i))
        time.sleep(1)


if __name__ == "__main__":
    ng = GProc(1, 300, ls=60, div=3.0)
    gp_sample = ng.sample_gp(300)

    # Plot both samples
    plt.subplot(111)
    plt.plot(np.arange(gp_sample.shape[1]), gp_sample[0], '-')
    plt.title('Gaussian process: \n $l_s=8, div=3$')
    plt.xlabel('steps [200ms]')
    plt.ylabel('Action noise value')
    #plt.suptitle('Random processes', fontsize=16)

    plt.subplots_adjust(left=0.15, wspace=0.4, top=0.9)

    plt.show()
