import torch.nn.functional as F
import torch.nn as nn
import torch

class CNNGSMapper(torch.nn.Module):
    def __init__(self):
        super(CNNGSMapper, self).__init__()

        # Initialize network layers here
        self.fc1 = nn.Linear(8, 48)
        # self.dropout = nn.Dropout(0.05)
        self.fc2 = nn.Linear(48, 4)

        # xavier initialization
        nn.init.xavier_normal_(self.fc1.weight)
        nn.init.xavier_normal_(self.fc2.weight)

    def forward(self, x):

        # x = torch.unsqueeze(x, 1)

        # x = self.dropout(x)
        x = F.relu(self.fc1(x))
        # x = self.dropout(x)
        x = self.fc2(x)
        return x


    def predict(self, x):
        '''
        Predict label probabilities for single image
        :param x: ndarray image of shape (h,w)
        :return: ndarray vector of probabilities of the labels
        '''

        # TODO: turn the x image into a torch tensor and add a batch dimension to the zeroth position
        # TODO: Do a forward pass through the network and use a F.softmax() to get the output probabilities
        # TODO: Turn the result to a numpy array by first detaching it: .detach() and then calling .numpy() on it
        x = torch.from_numpy(x)
        x = torch.unsqueeze(x, 0)
        return F.softmax(self.forward(x), 1)[0].detach().cpu().numpy()


    def trainCNN(self, DS, TST_DS, iters, batchsize, lr=1e-4):
        print("Training CNN")
        criterion = nn.MSELoss()
        optimizer = torch.optim.SGD(self.parameters(), lr, momentum=0.9)

        for i in range(iters):
            # TODO: dat nad for cklus
            coords, angles = DS.getBatch(batchsize)
            coords = torch.from_numpy(coords)
            angles = torch.from_numpy(angles)
            y = self.forward(coords)
            loss = criterion(y, angles)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            if i % 100 == 0:
                #TODO: Call the self.testCNN(TST_DS) function, passing the test dataset as an argument and print the loss
                tst_loss = self.testCNN(TST_DS)
                print("Iter {}/{}, trn_loss: {}, tst_loss: {}".format(i, iters, loss, tst_loss))

    def testCNN(self, DS):
        BSZ = 128  # batch size
        criterion = nn.KLDivLoss()
        coords, angles = DS.getBatch(BSZ)
        coords = torch.from_numpy(coords)
        angles = torch.from_numpy(angles)
        # cats = torch.from_numpy(cats)
        y = self.forward(coords)
        loss = criterion(y, angles)
        return loss

