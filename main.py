import numpy as np
from dataset import Dataset
from networks import CNNGSMapper
from cam_control import CamControl
from utils import countdown
import torch
import cv2
import matplotlib.pyplot as plt
import time
from kukamanip import Kuka

# Initialize webcam
#CAM = CamControl()

# Gesture list
gestures = ["front","back","right","left","up","down","noop"]
gestures_gather = ["front","back","right","left","up","down","noop"]

# Gather dataset of gestures
DS_trn = Dataset()
DS_tst = Dataset()
# DS_tst = Dataset("gr_test_set", gestures, res=(128,128))

# Script flags
GATHER_TRN = 1
TRAIN = 0
TEST = 0

LR = 1e-4

# Amount of train and test samples gathered
N_TRN = 5
# N_TST = 10

# print("zakladam robota")
# # ROB = rob.Mujocorobot()
# print("robot vytvoren")


CAM = 0
if GATHER_TRN:

    ROB = Kuka();
    # for i in range(len(gestures_gather)):
    DS_trn.gather(N_TRN, ROB)


    print("END GATHERING...")
    countdown(3)

# Make Classifier
CNN = CNNGSMapper()

if TRAIN:
    # Train
    ITERS = 10
    CNN.trainCNN(DS_trn, DS_tst, iters=ITERS, batchsize=1, lr=LR)
    torch.save(CNN, "model.pt")

if TEST:
    if not TRAIN:
        # load network trained on GPU
        device = torch.device('cpu')
        CNN.load_state_dict(torch.load("model.pt", map_location=device))

    # Test
    print("Testing CNN")
    testloss = CNN.testCNN(DS_trn)
    print("Test loss: {}".format(testloss))

    # while True:
    #     # Get image
    #     uint_img = CAM.snap((128,128))
    #
    #     # Normalize image
    #     img = cv2.normalize(uint_img.astype('float32'), None, 0.0, 1.0, cv2.NORM_MINMAX)
    #
    #     # Predict gesture
    #     gesture_softmax = CNN.predict(img)
    #     gesture_id = np.argmax(gesture_softmax)
    #
    #     # Arrow points
    #     pts = [(0,0),(0,0)]
    #
    #     #
    #     if gesture_id == 0:
    #         # front
    #         pts = [(70, 70), (50, 50)]
    #     if gesture_id == 1:
    #         # back
    #         pts = [(50, 50), (70, 70)]
    #     if gesture_id == 2:
    #         # right
    #         pts = [(40, 80), (88, 80)]
    #     if gesture_id == 3:
    #         # left
    #         pts = [(88, 80), (40, 80)]
    #     if gesture_id == 4:
    #         # up
    #         pts = [(64, 80), (64, 40)]
    #     if gesture_id == 5:
    #         # down
    #         pts = [(64, 40),(64, 80)]
    #     if gesture_id == 6:
    #         # noop
    #         pass
    #
    #     print(gestures[gesture_id])
    #
    #     # Draw arrow on the image
    #     img = cv2.arrowedLine(img, pts[0], pts[1], color=1,thickness=3)
    #
    #     # Render image
    #     cv2.imshow("webcam", img)
    #     if cv2.waitKey(1) == 27:
    #         break

        # Write the result to the video stream (optional)
        #CAM.video_out.write(cv2.cvtColor(uint_img,cv2.COLOR_GRAY2RGB))

