from cam_control import CamControl
import tf_pose
from tf_pose.estimator import TfPoseEstimator
import cv2
import time

class PoseEstimator:
    def __init__(self):
        pass

    def getBodyPose(self, img):
        return tf_pose.infer(img, model='mobilenet_thin')

    def getHandPose(self, img):
        raise NotImplementedError

    def visualizeBodyparts(self, img, bodyparts):
        body_image = TfPoseEstimator.draw_humans(img, bodyparts, imgcopy=True)
        cv2.imshow('tf-pose-estimation result', body_image)
        cv2.waitKey()

if __name__ == "__main__":
    time.sleep(3)
    cam = CamControl()
    img = cam.snap()

    pe = PoseEstimator()
    bodyparts = pe.getBodyPose(img)
    pe.visualizeBodyparts(img, bodyparts)




